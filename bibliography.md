# Visualizers
- https://json-schema.app/
	- Atlassian, doesn't eat our schema nicely
- https://json-schema-visualizer.netlify.app/
	- Tree rendering of entire schema
	- Handles conditionals
	- Thinks it's using https://github.com/mohsen1/json-schema-view-js
	- Also http://azimi.me/json-schema-view-js/demo/
- https://navneethg.github.io/jsonschemaviewer/
	- D3-based tree rendering of the schema (no conditionals)
- https://github.com/graviphoton/json-schema-visualizer
	- Angular, doesn't work on arm
- https://github.com/yanick/jsonschematic JSON Schema -> HTML rendering
	- 2 years old
- https://jsonschema.net/ Really good JSON -> JSON Schema generation and Schema editing

# Discovery tools
- https://github.com/SOM-Research/jsonDiscoverer (Java)
- https://github.com/SchemaStore/schemastore Loads of polished JSON Schemas for many web services, apis, file formats

# Schema Generation
- https://github.com/Nijikokun/generate-schema (Convert JSON to multiple output schema formats, MySQL table schema, JSON Schema, Mongoose, etc)
	- Fun to use
	- Obvs. can't infer validation patterns, requirements of conditionality of fields
- https://orderly-json.org/ (Superset of JSON Schema, designed to be easier to read)
	- Last update 2009, can't even support draft 04
	- Has grammar

# Docs
- https://json-schema.org/understanding-json-schema/index.html Understanding JSON Schema book
- http://json-schema.org/specification.html The JSON Schema specification
- https://github.com/CesiumGS/wetzel Markdown docs, supporting draft-07 with 'some limitations'

# Java
- https://github.com/worldturner/medeia-validator Validator supporting draft-04 draft-07
- https://github.com/victools/jsonschema-generator Schema generator supporting draft-06/07/2019-09/2020-12
- https://github.com/saasquatch/json-schema-inferrer Schema inferring from JSON supporting draft-04/07

# Kotlin
- https://github.com/pwall567/json-kotlin-schema-codegen Code generation from JSON Schema supporting draft-07


# OpenAPI
- "OpenAPI 3.0 uses an extended subset of [JSON Schema Specification](http://json-schema.org/) Wright Draft 00 (aka Draft 5) to describe the data formats. “Extended subset” means that some keywords are supported and some are not, some keywords have slightly different usage than in JSON Schema, and additional keywords are introduced."
- `discriminator` "To help API consumers detect the object type, you can add the `discriminator/propertyName` keyword to model definitions. This keyword points to the property that specifies the data type name"
- JSON Schema has introduced `propertyDependencies` to provide syntactic sugar over this pattern (it is already possible with `if/then/else` and depedant subschemas in draft-07) https://github.com/json-schema-org/json-schema-spec/pull/1143 https://github.com/json-schema-org/json-schema-spec/issues/1082
- API Docs https://stoplight.io/open-source/elements