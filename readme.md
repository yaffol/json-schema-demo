# JSON Schema Demo

This repo contains example JSON Schema and data files.

Schema are located in `schema/` and data in `json/`.

The basic example `RecordSchema.json` is based on the incomming Record file validation schema for the [Crossref Content Registration UI](https://gitlab.com/crossref/mycrossref), extended to show more schema features.

`csl-citation|data.json` show example schema from a similar domain to that of Crossref - in this case, Citation Style Language.

`scratch/` contains ephemeral files of passing interest, such as a very large schema `liquibase-3.2.json` and a start at abstracting definitions into a separate file `Common.json`

## Validation within VSCode

VSCode has good JSON support, and can [natively validate](https://code.visualstudio.com/docs/languages/json#_json-schemas-and-settings) JSON Schema within the editor. VSCode supports up to [draft-07](https://json-schema.org/draft-07/json-schema-release-notes.html) schema.

To enable validation within the editor, either include a `$schema` property within your JSON data file pointing to the JSON Schema file against which the data should be validated, or use the `json.schemas` property under the User or Workspace settings to setup file matching to specific JSON Schema. More information [here](https://code.visualstudio.com/docs/languages/json#_json-schemas-and-settings).