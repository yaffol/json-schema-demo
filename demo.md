1. filename
  * minLength
2. recordType
  * enum
3. schemaVersion
  * pattern
4. network_address
  * anyOf
  * factor
  * if/then/else
5. #/definitions
6. array
  * uniqueItems
7. patternProperties


